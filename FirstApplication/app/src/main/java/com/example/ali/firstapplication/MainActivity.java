package com.example.ali.firstapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    Button ok, cancell;
    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ok = (Button) findViewById(R.id.buttonOk);
        cancell = (Button) findViewById(R.id.buttonCancell);
        editText = (EditText) findViewById(R.id.description);

        ok.setOnClickListener(onClickListener);
        cancell.setOnClickListener(onClickListener);

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Toast.makeText(MainActivity.this, "description is : " + charSequence, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
//            if (view.equals(ok)); in tori ham mishe id ro peyda kard
//                Toast.makeText(MainActivity.this, "descriptioin : " + editText.getText().toString(), Toast.LENGTH_SHORT).show();
            switch (view.getId()) {
                case R.id.buttonOk:
                    Toast.makeText(MainActivity.this, "descriptioin : " + editText.getText().toString(), Toast.LENGTH_SHORT).show();
                    break;
                case R.id.buttonCancell:
                    Toast.makeText(MainActivity.this, "Cancell", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };
}
